#!/usr/bin/env bash
# Run postman/newman collection from <root>/tests/postman in docker (on a CI runner)

set -euo pipefail

# default test result is set to 1 = fail
test_result=1

cleanup() {
    if [[ "$test_result" -ne 0 ]]; then
        section "Logs"
        docker-compose -f ./test/docker-compose-CI.yml logs
    fi

    section "Cleaning up"
    docker-compose -f ./test/docker-compose-CI.yml down
    if [ "$test_result" = 0 ]; then
        fg_yellow OK; echo
    else
        fg_red ERROR; echo
    fi
}

# if we are interrupted, cleanup
trap cleanup EXIT
docker-compose -f ./test/docker-compose-CI.yml up -d metric-pusher
docker-compose -f ./test/docker-compose-CI.yml run integration_test-runner sh -c 'newman run -e postman/metric-pusher.env.postman_environment.json postman/miniSSBT-metric_pusher.postman_collection.json'
test_result=0
