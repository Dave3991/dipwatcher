﻿FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /src
COPY ["DipWatcher/DipWatcher.csproj", "DipWatcher/"]
RUN dotnet restore "DipWatcher/DipWatcher.csproj"
COPY . .
WORKDIR "/src/DipWatcher"
RUN dotnet build "DipWatcher.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "DipWatcher.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app

ARG COMMIT_SHA
ARG PROJECT_VERSION=0
ARG BUILD_ID
ARG PIPELINE
ARG GIT_TAG
ARG TIMESTAMP

ENV COMMIT_SHA="${COMMIT_SHA}" \
    PROJECT_VERSION="${PROJECT_VERSION}" \
    BUILD_ID="${BUILD_ID}" \
    PIPELINE="${PIPELINE}" \
    GIT_TAG="${GIT_TAG}" \
    TIMESTAMP="${TIMESTAMP}"

COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "DipWatcher.dll"]
HEALTHCHECK CMD curl --fail http://localhost:5000/health || exit