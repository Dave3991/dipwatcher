﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DipWatcher.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "prediction_result",
                columns: table => new
                {
                    result_id = table.Column<string>(type: "text", nullable: false),
                    date_time = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    is_anomaly = table.Column<bool>(type: "boolean", nullable: false),
                    prediction_value = table.Column<double>(type: "double precision", nullable: false),
                    value = table.Column<double>(type: "double precision", nullable: false),
                    anomaly_side = table.Column<byte>(type: "smallint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_prediction_result", x => new { x.result_id, x.date_time });
                });

            migrationBuilder.CreateTable(
                name: "stock_time_series",
                columns: table => new
                {
                    stock_symbol = table.Column<string>(type: "text", nullable: false),
                    time_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    open = table.Column<decimal>(type: "numeric", nullable: false),
                    high = table.Column<decimal>(type: "numeric", nullable: false),
                    low = table.Column<decimal>(type: "numeric", nullable: false),
                    close = table.Column<decimal>(type: "numeric", nullable: false),
                    volume = table.Column<decimal>(type: "numeric", nullable: false),
                    is_dip = table.Column<bool>(type: "boolean", nullable: false),
                    is_spike = table.Column<bool>(type: "boolean", nullable: false),
                    is_anomaly = table.Column<bool>(type: "boolean", nullable: false),
                    anomaly_probability = table.Column<double>(type: "double precision", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_stock_time_series", x => new { x.stock_symbol, x.time_utc });
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "prediction_result");

            migrationBuilder.DropTable(
                name: "stock_time_series");
        }
    }
}
