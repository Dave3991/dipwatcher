using System.IO;
using dotenv.net;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace DipWatcher
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            new HostBuilder()
                .ConfigureHostConfiguration(builder =>
                {
                    /* Host configuration */
                })
                .ConfigureAppConfiguration((host, builder) =>
                {
                    DotEnv.Load();
                    builder.SetBasePath(Directory.GetCurrentDirectory());
                    builder.AddJsonFile($"appsettings.{host.HostingEnvironment.EnvironmentName}.json", optional: false, reloadOnChange: true);
                    builder.AddEnvironmentVariables();
                    builder.AddCommandLine(args); ;
                })
                .ConfigureServices(services =>
                {
                    /* services configuration */
                })
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); })
                .UseSerilog((hostBuilder, configuration) =>
                {
                    configuration.ReadFrom.Configuration(hostBuilder.Configuration);
                }, preserveStaticLogger: true);
    }
}