using System;
using System.Threading;
using System.Threading.Tasks;
using App.Metrics;
using MediatR;
using Microsoft.Extensions.Logging;

namespace DipWatcher.Application.Mediator
{
    public class LoggingBehaviour<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse> where TRequest : notnull
    {
        private readonly ILogger<LoggingBehaviour<TRequest, TResponse>> _logger;

        private readonly IMetrics _metrics;
        public LoggingBehaviour(ILogger<LoggingBehaviour<TRequest, TResponse>> logger, IMetrics metrics)
        {
            _logger = logger;
            _metrics = metrics;
        }
        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            // https://dev.to/raulismasiukas/logging-pipeline-with-mediatr-5d4f
            var requestNameWithGuid = $"{typeof(TRequest).Name} [{Guid.NewGuid()}]";
            var tags = new MetricTags("request", typeof(TRequest).Name);
            TResponse response;

            _logger.LogInformation("[START] {RequestName}", requestNameWithGuid);
            using (_metrics.Measure.Timer.Time(MetricsOptions.MediatorRequestsExecutionTimer, tags))
            {
                try
                {
                    response = await next();
                }
                catch (Exception exception)
                {
                    _metrics.Measure.Meter.Mark(MetricsOptions.MediatorRequestsExceptions,
                        new MetricTags(new string[] { "request", "exception" },
                            new string[]
                            {
                                typeof(TRequest).Name, exception.GetType().FullName ?? "no exception full name"
                            }));
                    _logger.LogError(exception.Message, exception);
                    throw;
                }
                finally
                {
                    _logger.LogInformation("[END] {RequestName};", requestNameWithGuid);
                    _metrics.Measure.Meter.Mark(MetricsOptions.MediatorRequests, tags);
                }
            }

            return response;
        }
    }
}
