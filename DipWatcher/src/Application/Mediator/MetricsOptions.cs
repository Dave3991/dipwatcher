using App.Metrics;
using App.Metrics.Filters;
using App.Metrics.Meter;
using App.Metrics.Timer;

namespace DipWatcher.Application.Mediator
{
    public static class MetricsOptions
    {
        public static readonly TimerOptions MediatorRequestsExecutionTimer = new TimerOptions
        {
            Name = "Mediator Requests Execution Time",
            MeasurementUnit = App.Metrics.Unit.Requests,
            DurationUnit = TimeUnit.Milliseconds,
            RateUnit = TimeUnit.Seconds,
            ResetOnReporting = true
        };

        public static readonly MeterOptions MediatorRequests = new MeterOptions
        {
            Name = "Mediator Requests",
            MeasurementUnit = App.Metrics.Unit.Requests,
            RateUnit = TimeUnit.Seconds,
            ResetOnReporting = true
        };

        public static readonly MeterOptions MediatorRequestsExceptions = new MeterOptions
        {
            Name = "Mediator Requests Exceptions",
            MeasurementUnit = App.Metrics.Unit.Requests,
            RateUnit = TimeUnit.Seconds,
            ResetOnReporting = true
        };
    }
}
