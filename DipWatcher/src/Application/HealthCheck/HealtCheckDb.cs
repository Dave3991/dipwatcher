using DipWatcher.Infrastructure.Database;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DipWatcher.Application.HealthCheck
{
    public static class HealtCheckDb
    {
        public static void Configure(IServiceCollection services, IConfiguration configuration)
        {
            services.AddHealthChecks()
                //  Microsoft.Extensions.Diagnostics.HealthChecks.EntityFrameworkCore
                .AddDbContextCheck<DipWatcherDbContext>("database");
        }
    }
}