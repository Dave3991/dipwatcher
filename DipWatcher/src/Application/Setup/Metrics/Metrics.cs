using System.Linq;
using App.Metrics;
using App.Metrics.Formatters.Prometheus;
using Microsoft.Extensions.DependencyInjection;

namespace DipWatcher.Application.Setup.Metrics
{
    public static class Metrics
    {
        public static readonly IMetricsRoot MetricsConfiguration = AppMetrics.CreateDefaultBuilder().Build();
        public static void Configure(IServiceCollection services)
        {
            services.AddMetrics(options => // #nuget App.Metrics.AspNetCore.Core
            {
                var builder = options.Configuration.Builder;
                builder.Configuration.Configure(
                    options =>
                    {
                        options.AddServerTag();
                        options.Enabled = true;
                        options.GlobalTags.Add("app", "DipWatcher");
                    });
                builder.Report.ToConsole()
                    .OutputMetrics.AsPrometheusPlainText()


                    // .OutputMetrics.AsJson()
                    .Build();

            });
            services.AddAppMetricsCollectors();

            // #nuget App.Metrics.AspNetCore.Endpoints
            services.AddMetricsEndpoints(options =>
            {
                options.MetricsTextEndpointOutputFormatter = MetricsConfiguration.OutputMetricsFormatters.OfType<MetricsPrometheusTextOutputFormatter>().First();
                options.MetricsEndpointEnabled = true;
                options.EnvironmentInfoEndpointEnabled = true;
            });
        }
    }
}
