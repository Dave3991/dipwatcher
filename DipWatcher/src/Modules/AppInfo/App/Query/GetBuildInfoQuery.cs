using DipWatcher.Modules.AppInfo.Domain.ValueObject;
using MediatR;

namespace DipWatcher.Modules.AppInfo.App.Query
{
    public class GetBuildInfoQuery : IRequest<BuildInfo>
    {

    }
}