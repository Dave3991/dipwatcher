using DipWatcher.Modules.AppInfo.Domain.Case;
using DipWatcher.Modules.AppInfo.Domain.ValueObject;
using MediatR;

namespace DipWatcher.Modules.AppInfo.App.Query
{
    public class GetBuildInfoHandler : RequestHandler<GetBuildInfoQuery, BuildInfo>
    {
        private readonly GetBuildInfoCase _getBuildInfoCase;

        public GetBuildInfoHandler(GetBuildInfoCase getBuildInfoCase)
        {
            _getBuildInfoCase = getBuildInfoCase;
        }

        protected override BuildInfo Handle(GetBuildInfoQuery request)
        {
            return _getBuildInfoCase.GetBuildInfo();
        }
    }
}