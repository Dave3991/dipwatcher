using System;

namespace DipWatcher.Modules.AppInfo.Domain.Enum
{
    public enum BuildEnvEnum
    {
        CI_COMMIT_SHA,
        PROJECT_VERSION,
        BUILD_ID,
        BUILD_PIPELINE,
        BUILD_GIT_TAG,
        BUILD_TIMESTAMP
    }
}