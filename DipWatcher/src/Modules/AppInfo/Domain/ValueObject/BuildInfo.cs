using System.Collections;
using DipWatcher.Modules.AppInfo.Domain.Enum;

namespace DipWatcher.Modules.AppInfo.Domain.ValueObject
{
    public class BuildInfo
    {
        public string? ProjectBuild { get; }
        public string? ProjectVersion { get; }
        public string? BuildId { get; }
        public string? Pipeline { get; }
        public string? GitTag { get; }
        public string? Timestamp { get; }

        private BuildInfo(string? projectBuild, string? projectVersion, string? buildId, string? pipeline,
            string? gitTag, string? timestamp)
        {
            ProjectBuild = projectBuild;
            ProjectVersion = projectVersion;
            BuildId = buildId;
            Pipeline = pipeline;
            GitTag = gitTag;
            Timestamp = timestamp;
        }

        public static BuildInfo Create(IDictionary environmentVariables)
        {
            var projectBuild = environmentVariables[BuildEnvEnum.CI_COMMIT_SHA]?.ToString();
            var projectVersion = environmentVariables[BuildEnvEnum.PROJECT_VERSION]?.ToString();
            var buildId = environmentVariables[BuildEnvEnum.BUILD_ID]?.ToString();
            var pipeline = environmentVariables[BuildEnvEnum.BUILD_PIPELINE]?.ToString();
            var gitTag = environmentVariables[BuildEnvEnum.BUILD_GIT_TAG]?.ToString();
            var timestamp = environmentVariables[BuildEnvEnum.BUILD_TIMESTAMP]?.ToString();

            return new BuildInfo(projectBuild, projectVersion, buildId, pipeline, gitTag, timestamp);
        }
    }
}