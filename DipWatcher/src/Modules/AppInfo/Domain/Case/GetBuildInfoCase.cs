using System;
using DipWatcher.Modules.AppInfo.Domain.Enum;
using DipWatcher.Modules.AppInfo.Domain.ValueObject;

namespace DipWatcher.Modules.AppInfo.Domain.Case
{
    public class GetBuildInfoCase
    {
        public BuildInfo GetBuildInfo()
        {
            var env = Environment.GetEnvironmentVariables();
            return BuildInfo.Create(env);
        }

    }
}