using DipWatcher.Modules.AppInfo.Domain.Case;
using Microsoft.Extensions.DependencyInjection;

namespace DipWatcher.Modules.AppInfo.Setup
{
    public static class Services
    {
        public static void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<GetBuildInfoCase>();
        }
    }
}