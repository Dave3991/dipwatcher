using App.Metrics;
using DipWatcher.Modules.AppInfo.App.Query;
using DipWatcher.Modules.AppInfo.Domain.ValueObject;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace DipWatcher.Modules.AppInfo.UI
{
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class InfoController : Controller
    {
        private readonly IMediator _mediator;

        private readonly IMetrics _metrics;

        public InfoController(IMediator mediator, IMetrics metrics)
        {
            _mediator = mediator;
            _metrics = metrics;
        }

        /// <summary>
        /// Returns the application info
        /// </summary>
        [HttpGet("build-info")]
        public BuildInfo BuildInfo()
        {
            return _mediator.Send(new GetBuildInfoQuery()).Result;
        }

        [HttpGet("metrics")]
        public string Metrics()
        {
            var snapshot = _metrics.Snapshot.Get();
            // using (var stream = new MemoryStream())
            // {
            //     await _metrics.DefaultOutputMetricsFormatter.WriteAsync(stream, snapshot);
            //     var result = Encoding.UTF8.GetString(stream.ToArray());
            //     System.Console.WriteLine(result);
            // }
            return snapshot.Timestamp.ToLongDateString();
        }
    }
}