using System;

namespace DipWatcher.Modules.DataCollector.Domain.ValueObject
{
    public record StockData
    {
        public string StockSymbol { get; }
        public DateTime TimeStamp { get; }
        public decimal Open { get; }
        public decimal High { get; }
        public decimal Low { get; }
        public decimal Close { get; }
        public decimal Volume { get; }
        public decimal Vwap { get; }
        public decimal TradeCount { get; }

        public StockData
        (
            string stockSymbol,
            DateTime timeStamp,
            decimal open,
            decimal high,
            decimal low,
            decimal close,
            decimal volume,
            decimal vwap,
            decimal tradeCount
        )
        {
            StockSymbol = stockSymbol ?? throw new ArgumentNullException(nameof(stockSymbol));
            TimeStamp = timeStamp;
            Open = open;
            High = high;
            Low = low;
            Close = close;
            Volume = volume;
            Vwap = vwap;
            TradeCount = tradeCount;
        }
    }
}