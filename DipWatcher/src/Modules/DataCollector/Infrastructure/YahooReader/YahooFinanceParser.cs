using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net.Http;
using System.Text;
using CsvHelper;
using CsvHelper.Configuration;
using DipWatcher.Modules.DataCollector.Infrastructure.YahooReader.CsvMap;
using DipWatcher.Modules.DataCollector.Infrastructure.YahooReader.Dto;
using Microsoft.AspNetCore.WebUtilities;

namespace DipWatcher.Modules.DataCollector.Infrastructure.YahooReader
{
    public class YahooFinanceParser
    {
        public IEnumerable<StockDatetimePriceDto> LoadJson(string stockSymbol)
        {
            _ = (Int32)(DateTime.UtcNow.Subtract(DateTime.Now.AddYears(-4))).TotalSeconds; //from last 4 years
            var timeStampNow = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            using var httpClient = new HttpClient();
            using var request = new HttpRequestMessage(
                new HttpMethod("GET"),
                new Uri($"https://query1.finance.yahoo.com/v7/finance/download/{stockSymbol}?period1=1624233600&period2={timeStampNow}&interval=1d&events=history&includeAdjustedClose=true")
                );
            request.Headers.TryAddWithoutValidation("Upgrade-Insecure-Requests", "1");

            var response = httpClient.Send(request);
            if (response.Content is null)
            {
                throw new Exception("Content is null");
            }

            var stream = response.Content.ReadAsStream();
            if (stream == null)
            {
                throw new Exception("stream is null");
            }
            TextReader reader = new HttpRequestStreamReader(stream, Encoding.UTF8);
            // reader.ReadLine();
            var csvConfiguration = new CsvConfiguration(CultureInfo.CurrentCulture);
            var csvParser = new CsvParser(reader, csvConfiguration);
            var csvReader = new CsvReader(csvParser);
            csvReader.Context.RegisterClassMap<YahooFinanceMap>();
            var records = csvReader.GetRecords<StockDatetimePriceDto>();
            return records;

        }
    }
}