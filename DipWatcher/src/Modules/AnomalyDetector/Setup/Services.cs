using DipWatcher.Modules.AnomalyDetector.Application.Service;
using DipWatcher.Modules.AnomalyDetector.Domain;
using DipWatcher.Modules.AnomalyDetector.Domain.Case;
using DipWatcher.Modules.AnomalyDetector.Domain.Services;
using DipWatcher.Modules.AnomalyDetector.Infrastructure.AlpacaReader;
using DipWatcher.Modules.AnomalyDetector.Infrastructure.Database;
using DipWatcher.Modules.AnomalyDetector.Infrastructure.FileWriter;
using DipWatcher.Modules.AnomalyDetector.Infrastructure.SpreadSheet;
using DipWatcher.Modules.AnomalyDetector.Infrastructure.YahooReader;
using Microsoft.Extensions.DependencyInjection;

namespace DipWatcher.Modules.AnomalyDetector.Setup
{
    public static class Services
    {
        public static void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<ChangePointDetectionCase>();
            services.AddScoped<SpikeDetectionSsaCase>();
            services.AddScoped<SpikeDetectionCase>();
            services.AddScoped<AnomalyDetectionService>();
            services.AddScoped<YahooFinanceParser>();
            services.AddScoped<AlpacaParser>();
            services.AddScoped<TweetSender.Case.TweetSenderCase>();
            services.AddScoped<ConvertInfrastructureData>();
            services.AddScoped<FileWriter>();
            services.AddScoped<StockRepository>();
            services.AddScoped<IPredictionResultRepository,PredictionRepository>();
            services.AddScoped<SpreadSheet>();
            services.AddScoped<Authentication>();
        }
    }
}
