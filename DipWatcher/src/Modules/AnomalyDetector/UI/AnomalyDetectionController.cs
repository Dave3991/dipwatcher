using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using App.Metrics;
using DipWatcher.Modules.AnomalyDetector.Application.Operations;
using DipWatcher.Modules.AnomalyDetector.Domain.Entity;
using DipWatcher.Modules.AnomalyDetector.Domain.ValueObject;
using DipWatcher.Modules.AnomalyDetector.Infrastructure.FileWriter;
using DipWatcher.Modules.AnomalyDetector.Infrastructure.YahooReader;
using DipWatcher.Modules.AnomalyDetector.Infrastructure.YahooReader.Dto;
using DipWatcher.Modules.AnomalyDetector.UI.Dto;
using DipWatcher.Modules.MessageProvider.Application.Operations;
using DipWatcher.Modules.MessageProvider.Domain.Dto;
using DipWatcher.Modules.TweetSender.Application.Operations;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.ML.Transforms.TimeSeries;

namespace DipWatcher.Modules.AnomalyDetector.UI
{

    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class AnomalyDetectionController : Controller
    {
        private readonly IMediator _mediator;

        private readonly FileWriter _fileWriter;

        public AnomalyDetectionController(IMediator mediator, FileWriter fileWriter)
        {
            _mediator = mediator;
            _fileWriter = fileWriter;
        }
        /// <summary>
        /// Detects spike from given data
        /// </summary>
        [HttpPost("/spike-detect-external-data")]
        public IEnumerable<PredictionResultEntity>? SpikeDetectExternalData(IEnumerable<TimeSeriesData> timeSeriesDatas)
        {
            var resultId = "detect_spike_from_give_data";
            return _mediator.Send(new SpikeDetectExternalDataCommand(resultId,timeSeriesDatas)).Result;
        }
        /// <summary>
        /// Find change point from given data
        /// </summary>
        [HttpPost("/change-point-detect-external-data")]
        public IEnumerable<TimeSeriesDataPrediction>? ChangePointDetectExternalData(IEnumerable<TimeSeriesData> timeSeriesDatas)
        {
            return _mediator.Send(new ChangePointDetectExternalDataCommand(timeSeriesDatas)).Result;
        }


        /// <summary>
        /// Detects spike from specified data endpoint
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /spike-detect
        ///     {
        ///        "StockSymbol": "aapl",
        ///        "UseSsaMethod": false,
        ///        "Confidence": 95.0,
        ///        "dataSource": "Yahoo",
        ///        "anomalySide": "Negative"
        ///     }
        /// 
        /// </remarks>
        /// <example>Jane</example>
        /// <returns>returns computed data</returns>
        [HttpPost("/spike-detect")]
        public IEnumerable<PredictionResultEntity> SpikeDetect(ChangePointDetectRequest detectRequest)
        {
            var spikeData = _mediator.Send(new SpikeDetectYahooCommand(detectRequest)).Result;
            var lastSpike = (spikeData ?? throw new InvalidOperationException()).Last();
            if (lastSpike.IsAnomaly)
            {
                var msg = _mediator.Send(new GetMessageQuery(new TimeSeriesSpikeDto(
                    lastSpike.DateTime,
                    lastSpike.Value,
                    detectRequest.AnomalySide,
                    detectRequest.StockSymbol
                    ))).Result;
                _fileWriter.FileWrite(msg.Message);
            }
            return spikeData;
        }

        /// <summary>
        /// testing api endpoint
        /// </summary>
        /// <remarks>
        /// <param>
        ///     <name>StockSymbol</name>
        ///     <name>anomalySide</name>
        /// </param>
        /// Sample request:
        /// 
        ///     POST /spike-detect-alpaca
        ///     {
        ///        "StockSymbol": "aapl",
        ///        "UseSsaMethod": false,
        ///        "Confidence": 95.0,
        ///        "dataSource": "Yahoo",
        ///        "anomalySide": "Positive"
        ///     }
        /// 
        /// </remarks>
        /// <returns></returns>
        [HttpPost("/spike-detect-alpaca")]
        [Consumes("application/json")]
        public IEnumerable<PredictionResultEntity> SpikeDetectAlpaca(ChangePointDetectRequest detectRequest)
        {
            var spikeData = _mediator.Send(new SpikeDetectAlpacaCommand(detectRequest)).Result;
            var lastSpike = spikeData.Last();
            if (lastSpike.IsAnomaly)
            {
                _ = $"detected Dip for #{detectRequest.StockSymbol} at {Math.Round(lastSpike.Value, 2)} in {lastSpike.DateTime} #dipWatcher";
                // mediator.Send(new SendTweetCommand(msg));
            }
            return spikeData;
        }
    }
}
