using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using DipWatcher.Modules.AnomalyDetector.UI.Enum;
using Microsoft.ML.Transforms.TimeSeries;

namespace DipWatcher.Modules.AnomalyDetector.UI.Dto
{
    public class ChangePointDetectRequest
    {
        /// <example>AAPL</example>
        [Required]
        [DefaultValue("AAPL")]
        public string StockSymbol { get; }
        [Required]
        [DefaultValue(false)]
        public bool UseSsaMethod { get; }

        [Required]
        [DefaultValue("Yahoo")]
        public DataSource DataSource { get; }

        [Required]
        [DefaultValue(AnomalySide.Negative)]
        public AnomalySide AnomalySide { get; }

        [Required]
        [DefaultValue(95.0)]
        public double Confidence { get; }


        public ChangePointDetectRequest(
            bool useSsaMethod,
            string stockSymbol,
            AnomalySide anomalySide,
            double confidence,
            DataSource dataSource)
        {
            UseSsaMethod = useSsaMethod;
            StockSymbol = stockSymbol;
            AnomalySide = anomalySide;
            Confidence = confidence;
            DataSource = dataSource;
        }
    }
}