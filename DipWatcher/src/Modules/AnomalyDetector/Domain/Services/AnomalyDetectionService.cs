using System.Collections.Generic;
using DipWatcher.Modules.AnomalyDetector.Domain.ValueObject;
using Microsoft.ML;

namespace DipWatcher.Modules.AnomalyDetector.Domain.Services
{
    public class AnomalyDetectionService
    {
        public IDataView CreateEmptyDataView(MLContext mlContext)
        {
            // Create empty DataView. We just need the schema to call Fit() for the time series transforms
            IEnumerable<TimeSeriesData> enumerableData = new List<TimeSeriesData>();
            return mlContext.Data.LoadFromEnumerable(enumerableData);
        }
    }
}
