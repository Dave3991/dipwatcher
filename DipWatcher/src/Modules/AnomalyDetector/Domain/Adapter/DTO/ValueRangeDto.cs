using Google.Apis.Sheets.v4.Data;

namespace DipWatcher.Modules.AnomalyDetector.Domain.Adapter.DTO
{
    public class ValueRangeDto
    {
        public ValueRange ValueRange { get; }
        public int RowCount { get; }
        public int ColumnCount { get; }

        public ValueRangeDto(ValueRange valueRange, int rowCount, int columnCount)
        {
            ValueRange = valueRange;
            RowCount = rowCount;
            ColumnCount = columnCount;
        }

        public bool IsValid()
        {
            return ValueRange != null && RowCount > 0 && ColumnCount > 0;
        }
    }
}