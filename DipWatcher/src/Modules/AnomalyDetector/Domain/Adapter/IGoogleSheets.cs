using System.Collections.Generic;
using DipWatcher.Modules.AnomalyDetector.Domain.Adapter.DTO;
using Google.Apis.Sheets.v4.Data;

namespace DipWatcher.Modules.AnomalyDetector.Domain.Adapter
{
    public interface IGoogleSheets
    {
        public Spreadsheet CreateNewSpreadSheet(string spreadSheetName);
        public BatchUpdateSpreadsheetResponse AddSheet(Spreadsheet spreadsheet, string sheetName);
        public bool SheetExists(Spreadsheet spreadsheet, string sheetName);
        public Spreadsheet GetSpreadSheet(string spreadSheetId);
        public UpdateValuesResponse UpdateSpreadSheet(Spreadsheet spreadsheet, ValueRange rangeBody, string sheetname);

        /// <summary>
        /// Creates ValueRange, which is range filled with values
        /// </summary>
        /// <param name="values"></param>
        /// <param name="addColumnNames">add header to table</param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public ValueRangeDto CreateValueRange<T>(IEnumerable<T> values, bool addColumnNames = true);

        /// <summary>
        /// Get range which will be updated in the spreadsheet by given data
        /// </summary>
        /// <param name="sheetName"></param>
        /// <param name="data"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public string GetRange<T>(string sheetName, IEnumerable<T> data);

        public string GetRange<T>(string sheetName, IEnumerable<IEnumerable<T>> data);

        /// <summary>
        /// Get values from given range in the spreadsheet
        /// </summary>
        /// <param name="spreadsheet"></param>
        /// <param name="range"></param>
        /// <returns></returns>
        public ValueRange? GetValuesInRange(Spreadsheet spreadsheet, string range);

        /// <summary>
        /// Delete sheet from the spreadsheet
        /// </summary>
        /// <param name="spreadsheet"></param>
        /// <param name="sheet"></param>
        /// <returns></returns>
        public BatchUpdateSpreadsheetResponse DeleteSheet(Spreadsheet spreadsheet, Sheet sheet);




    }
}