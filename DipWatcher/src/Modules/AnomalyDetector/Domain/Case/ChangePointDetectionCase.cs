using System.Collections.Generic;
using System.Linq;
using DipWatcher.Modules.AnomalyDetector.Domain.Services;
using DipWatcher.Modules.AnomalyDetector.Domain.ValueObject;
using Microsoft.ML;

namespace DipWatcher.Modules.AnomalyDetector.Domain.Case
{
    public class ChangePointDetectionCase
    {
        private readonly AnomalyDetectionService _anomalyDetectionService;

        public ChangePointDetectionCase(AnomalyDetectionService anomalyDetectionService)
        {
            _anomalyDetectionService = anomalyDetectionService;
        }

        public IEnumerable<TimeSeriesDataPrediction>? DetectChangePoint(IEnumerable<TimeSeriesData> timeSeriesDatas)
        {
            var mlContext = new MLContext();
            var seriesDatas = timeSeriesDatas.ToList();
            IDataView dataView = mlContext.Data.LoadFromEnumerable(seriesDatas);
            var iidChangePointEstimator = mlContext.Transforms.DetectIidChangePoint(
                outputColumnName: nameof(TimeSeriesDataPrediction.Prediction),
                inputColumnName: nameof(TimeSeriesData.Value),
                confidence: 95.0,
                changeHistoryLength: seriesDatas.Count / 4);

            var iidChangePointTransform = iidChangePointEstimator.Fit(_anomalyDetectionService.CreateEmptyDataView(mlContext));
            IDataView transformedData = iidChangePointTransform.Transform(dataView);
            return mlContext.Data.CreateEnumerable<TimeSeriesDataPrediction>(transformedData, reuseRowObject: false);
        }
    }
}
