using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Reflection.Metadata;
using DipWatcher.Modules.AnomalyDetector.Domain.Entity;
using DipWatcher.Modules.AnomalyDetector.Domain.Services;
using DipWatcher.Modules.AnomalyDetector.Domain.ValueObject;
using Microsoft.ML;
using Microsoft.ML.Data;
using Microsoft.ML.Transforms.TimeSeries;

namespace DipWatcher.Modules.AnomalyDetector.Domain.Case
{
    // https://docs.microsoft.com/en-us/dotnet/machine-learning/tutorials/sales-anomaly-detection
    // https://github.com/dotnet/docs/blob/main/docs/machine-learning/tutorials/snippets/sales-anomaly-detection/csharp/Program.cs
    public class SpikeDetectionCase
    {
        private readonly AnomalyDetectionService _anomalyDetectionService;

        public SpikeDetectionCase(AnomalyDetectionService anomalyDetectionService)
        {
            _anomalyDetectionService = anomalyDetectionService;
        }

        public IEnumerable<PredictionResultEntity> DetectSpike(IEnumerable<TimeSeriesData> timeSeriesDatas,
            SpikeDetectionCaseConfig spikeDetectionCaseConfig)
        {
            var mlContext = new MLContext();
            var seriesDatas = timeSeriesDatas.ToImmutableArray();
            IDataView dataView = mlContext.Data.LoadFromEnumerable(seriesDatas);
            var iidSpikeEstimator = mlContext.Transforms.DetectIidSpike(
                outputColumnName: nameof(TimeSeriesDataPrediction.Prediction),
                inputColumnName: nameof(TimeSeriesData.Value),
                confidence: spikeDetectionCaseConfig.Confidence,
                pvalueHistoryLength: (int)(seriesDatas.LongCount() / 4),
                side: spikeDetectionCaseConfig.AnomalySide // upozornuje jen na spike dolu
            );
            ITransformer iidSpikeTransform =
                iidSpikeEstimator.Fit(_anomalyDetectionService.CreateEmptyDataView(mlContext));
            IDataView transformedData = iidSpikeTransform.Transform(dataView);
            var spikes =
                mlContext.Data.CreateEnumerable<TimeSeriesDataPrediction>(transformedData, reuseRowObject: false);
            return spikes.Zip(seriesDatas, (s, t) => new PredictionResultEntity(
                spikeDetectionCaseConfig.ResultId,
                s.Prediction[0] != 0,
                s.Prediction[2],
                t.DateTime,
                t.Value,
                spikeDetectionCaseConfig.AnomalySide
            ));
        }
    }
}