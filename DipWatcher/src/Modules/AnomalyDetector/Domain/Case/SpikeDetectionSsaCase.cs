using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Reflection.Metadata;
using DipWatcher.Modules.AnomalyDetector.Domain.Entity;
using DipWatcher.Modules.AnomalyDetector.Domain.Services;
using DipWatcher.Modules.AnomalyDetector.Domain.ValueObject;
using Microsoft.ML;
using Microsoft.ML.Data;
using Microsoft.ML.Transforms.TimeSeries;

namespace DipWatcher.Modules.AnomalyDetector.Domain.Case
{
    // https://docs.microsoft.com/en-us/dotnet/machine-learning/tutorials/sales-anomaly-detection
    // https://github.com/dotnet/docs/blob/main/docs/machine-learning/tutorials/snippets/sales-anomaly-detection/csharp/Program.cs
    public class SpikeDetectionSsaCase
    {
        private readonly AnomalyDetectionService _anomalyDetectionService;

        // Configure the Estimator
        const int PValueSize = 30;
        const int SeasonalitySize = 30;
        const int TrainingSize = 90;
        const int ConfidenceInterval = 98;

        public SpikeDetectionSsaCase(AnomalyDetectionService anomalyDetectionService)
        {
            _anomalyDetectionService = anomalyDetectionService;
        }

        [System.Obsolete]
        public IEnumerable<PredictionResultEntity> DetectSpike(IEnumerable<TimeSeriesData> timeSeriesDatas, SpikeDetectionCaseConfig spikeDetectionCaseConfig)
        {
            var mlContext = new MLContext();
            var seriesDatas = timeSeriesDatas.ToImmutableArray();
            IDataView dataView = mlContext.Data.LoadFromEnumerable(seriesDatas);
            var iidSpikeEstimator = mlContext.Transforms.DetectSpikeBySsa(
                    outputColumnName: nameof(TimeSeriesDataPrediction.Prediction),
                    inputColumnName: nameof(TimeSeriesData.Value),
                    confidence: ConfidenceInterval,
                    pvalueHistoryLength: PValueSize,
                    trainingWindowSize: TrainingSize,
                    seasonalityWindowSize: SeasonalitySize,
                    spikeDetectionCaseConfig.AnomalySide
            );
            ITransformer iidSpikeTransform = iidSpikeEstimator.Fit(_anomalyDetectionService.CreateEmptyDataView(mlContext));
            IDataView transformedData = iidSpikeTransform.Transform(dataView);
            var spikes = mlContext.Data.CreateEnumerable<TimeSeriesDataPrediction>(transformedData, reuseRowObject: false);
            return spikes.Zip(seriesDatas, (s, t) => new PredictionResultEntity(
                spikeDetectionCaseConfig.ResultId,
                s.Prediction[0] != 0,
                s.Prediction[2],
                t.DateTime,
                t.Value,
                spikeDetectionCaseConfig.AnomalySide
            ));
        }


    }
}
