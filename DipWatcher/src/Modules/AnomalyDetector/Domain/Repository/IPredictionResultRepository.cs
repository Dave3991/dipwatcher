using System.Collections.Generic;
using DipWatcher.Modules.AnomalyDetector.Domain.Entity;

namespace DipWatcher.Modules.AnomalyDetector.Domain
{
    public interface IPredictionResultRepository
    {
        public void AddPredictionResults(IEnumerable<PredictionResultEntity> predictionResult);
        public void AddOrUpdate(IEnumerable<PredictionResultEntity> predictionResult);
    }
}