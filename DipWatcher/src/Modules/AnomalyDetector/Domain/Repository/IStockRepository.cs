using System.Threading.Tasks;
using DipWatcher.Modules.AnomalyDetector.Domain.Entity;

namespace DipWatcher.Modules.AnomalyDetector.Domain
{
    public interface IStockRepository
    {
        Task<StockTimeSeriesEntity> GetStock(string symbol);
    }
}