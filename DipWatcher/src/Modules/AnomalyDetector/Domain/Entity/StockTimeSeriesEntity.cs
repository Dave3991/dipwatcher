using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DipWatcher.Modules.AnomalyDetector.Domain.Entity
{
    public class StockTimeSeriesEntity
    {
        // https://www.entityframeworktutorial.net/code-first/column-dataannotations-attribute-in-code-first.aspx
        [Key]
        [Column("stock_symbol")]
        public string StockSymbol { get; set; }

        [Key]
        [Column("time_utc")]
        public DateTime DateTime { get; set; }

        [Column("open")]
        public decimal Open { get; set; }

        [Column("high")]
        public decimal High { get; set; }

        [Column("low")]
        public decimal Low { get; set; }

        [Column("close")]
        public decimal Close { get; set; }

        [Column("volume")]
        public decimal Volume { get; set; }

        [Column("is_dip")]
        public bool IsDip { get; set; }

        [Column("is_spike")]
        public bool IsSpike { get; set; }

        [Column("is_anomaly")]
        public bool IsAnomaly { get; set; }

        [Column("anomaly_probability")]
        public double AnomalyProbability { get; set; }
    }
}