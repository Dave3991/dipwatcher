using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.ML.Transforms.TimeSeries;

namespace DipWatcher.Modules.AnomalyDetector.Domain.Entity
{
    [Table("prediction_result")]
    public class PredictionResultEntity
    {
        [Key]
        [Column("result_id")]
        public string ResultId { get; init; }
        [Column("is_anomaly")]
        public bool IsAnomaly { get; init; }

        // how likely is this spike, the lower value the more likely is it spike
        [Column("prediction_value")]
        public double PredictionValue { get; init; }

        [Key]
        [Column("date_time")]
        public DateTime DateTime { get; init; }

        // Value with which the prediction was made
        [Column("value")]
        public double Value { get; init; }

        [Column("anomaly_side")]
        public AnomalySide AnomalySide { get; init; }

        public PredictionResultEntity(string resultId, bool isAnomaly, double predictionValue, DateTime dateTime, double value, AnomalySide anomalySide)
        {
            ResultId = resultId;
            IsAnomaly = isAnomaly;
            PredictionValue = predictionValue;
            DateTime = dateTime;
            Value = value;
            AnomalySide = anomalySide;
        }

        public PredictionResultEntity()
        {
        }
    }
}