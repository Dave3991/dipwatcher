using System;

namespace DipWatcher.Modules.AnomalyDetector.Domain.ValueObject
{
    public class SignPostTickerRow
    {
        public string Ticker { get; }
        public DateTime UpdateTime { get; }
        public bool   IsDipDetected { get; }
        public double Price { get; }

        public SignPostTickerRow(string ticker, DateTime updateTime, bool isDipDetected, double price)
        {
            Ticker = ticker;
            UpdateTime = updateTime;
            IsDipDetected = isDipDetected;
            Price = price;
        }
    }
}