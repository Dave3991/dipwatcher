using Microsoft.ML.Transforms.TimeSeries;

namespace DipWatcher.Modules.AnomalyDetector.Domain.ValueObject
{
    public class SpikeDetectionCaseConfig
    {
        public string ResultId { get; }
        public AnomalySide AnomalySide { get; }

        public double Confidence { get; }

        public SpikeDetectionCaseConfig(string resultId, AnomalySide anomalySide, double confidence)
        {
            ResultId = resultId;
            AnomalySide = anomalySide;
            Confidence = confidence;
        }
    }
}