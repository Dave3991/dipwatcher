using System;
using Microsoft.ML.Transforms.TimeSeries;

namespace DipWatcher.Modules.AnomalyDetector.Domain.ValueObject
{
    public class TimeSeriesData
    {
        public DateTime DateTime { init; get; }
        public float Value { init; get; }

        public TimeSeriesData(DateTime dateTime, float value)
        {
            this.DateTime = dateTime;
            this.Value = value;
        }
    }
}
