using Microsoft.ML.Data;

namespace DipWatcher.Modules.AnomalyDetector.Domain.ValueObject
{
    public class TimeSeriesDataPrediction
    {
        //vector to hold alert,score,p-value values
        [VectorType(3)]
        public double[]? Prediction { get; set; }
    }
}
