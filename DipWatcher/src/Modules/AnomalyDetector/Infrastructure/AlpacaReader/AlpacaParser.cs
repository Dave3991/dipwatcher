using System;
using System.Collections.Generic;
using Alpaca.Markets;

namespace DipWatcher.Modules.AnomalyDetector.Infrastructure.AlpacaReader
{
    public class AlpacaParser
    {
        private static readonly string ApiKey = "PKJWOXHFOXUQB2UU92E0";//Environment.GetEnvironmentVariable("ALPACA_API_KEY") ?? throw new InvalidOperationException();

        private static readonly string ApiSecret = "7ijfttfSBIMbAHG59ezCURNmlvLcQmNO0ZYuc9oG"; // Environment.GetEnvironmentVariable("ALPACA_API_SECRET") ?? throw new InvalidOperationException();

        private readonly IAlpacaDataClient _tradingClient;

        private IAlpacaDataClient GetAlpacaClient()
        {
            return Alpaca.Markets.Environments.Paper.GetAlpacaDataClient(
                new SecretKey(ApiKey, ApiSecret)
            );
        }

        public AlpacaParser()
        {
            _tradingClient = GetAlpacaClient();
        }

        public IReadOnlyList<IBar> LoadJson(string stockSymbol)
        {
            var bars = _tradingClient.ListHistoricalBarsAsync(
                new HistoricalBarsRequest(stockSymbol, DateTime.Now.AddMonths(-2), DateTime.Now.AddMinutes(-30), BarTimeFrame.Hour)
                ).Result;

            return bars.Items;
        }

    }
}