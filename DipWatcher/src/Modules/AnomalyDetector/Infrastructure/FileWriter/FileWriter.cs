using System;
using System.IO;

namespace DipWatcher.Modules.AnomalyDetector.Infrastructure.FileWriter
{
    public class FileWriter
    {
        public void FileWrite(string text)
        {
            using StreamWriter file = new("./dips/dips-" + DateTime.Today.ToString("yyyy-M-d") + ".txt", append: true);
            file.WriteLine(text);
        }
    }
}