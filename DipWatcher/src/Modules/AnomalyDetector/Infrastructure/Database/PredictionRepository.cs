using System.Collections.Generic;
using DipWatcher.Infrastructure.Database;
using DipWatcher.Modules.AnomalyDetector.Domain;
using DipWatcher.Modules.AnomalyDetector.Domain.Entity;
using Microsoft.EntityFrameworkCore;

namespace DipWatcher.Modules.AnomalyDetector.Infrastructure.Database
{
    public class PredictionRepository : IPredictionResultRepository
    {
        private readonly DipWatcherDbContext _context;

        public PredictionRepository(DipWatcherDbContext context)
        {
            _context = context;
        }

        public void AddPredictionResults(IEnumerable<PredictionResultEntity> predictionResult)
        {
            foreach (var result in predictionResult)
            {
                _context.PredictionResult.Add(result);
            }
            _context.SaveChanges();
        }

        public void AddOrUpdate(IEnumerable<PredictionResultEntity> predictionResult)
        {
            foreach (var result in predictionResult)
            {
                _context.PredictionResult.FromSqlRaw("SELECT * FROM prediction_result_insert_or_update({0},{1},{2},{3},{4},{5})",
                    result.ResultId,
                    result.IsAnomaly,
                    result.PredictionValue,
                    result.DateTime,
                    result.Value,
                    result.AnomalySide
                ).LoadAsync();
            }
            _context.SaveChanges();
        }
    }
}