using System.Collections.Generic;
using System.Threading.Tasks;
using DipWatcher.Infrastructure.Database;
using DipWatcher.Modules.AnomalyDetector.Domain;
using DipWatcher.Modules.AnomalyDetector.Domain.Entity;
using Microsoft.EntityFrameworkCore;

namespace DipWatcher.Modules.AnomalyDetector.Infrastructure.Database
{
    public class StockRepository: IStockRepository{
        
        private readonly DipWatcherDbContext _context;

        public StockRepository(DipWatcherDbContext context)
        {
            _context = context;
        }

        public async Task<StockTimeSeriesEntity> GetStock(string symbol)
        {
            return await _context.stock_time_series.FirstOrDefaultAsync(s => s.StockSymbol == symbol);
        }

        public async Task<IEnumerable<StockTimeSeriesEntity>> GetStocks()
        {
            return await _context.stock_time_series.ToListAsync();
        }

        public void AddStock(IEnumerable<StockTimeSeriesEntity> stock)
        {
            foreach (var s in stock)
            {
                _context.stock_time_series.Add(s);
            }
            _context.SaveChanges();
        }

        // public async Task<StockTimeSeriesEntity> AddOrUpdateStock(StockTimeSeriesEntity stock)
        // {
        //
        // }

        public async Task<StockTimeSeriesEntity> UpdateStock(StockTimeSeriesEntity stock)
        {
            _context.stock_time_series.Update(stock);
            await _context.SaveChangesAsync();
            return stock;
        }

        public async Task<StockTimeSeriesEntity> DeleteStock(StockTimeSeriesEntity stock)
        {
            _context.stock_time_series.Remove(stock);
            await _context.SaveChangesAsync();
            return stock;
        }
    }    
}