using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DipWatcher.Modules.AnomalyDetector.Domain.Adapter;
using DipWatcher.Modules.AnomalyDetector.Domain.Adapter.DTO;
using DipWatcher.Modules.AnomalyDetector.Infrastructure.SpreadSheet.Exception;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;

namespace DipWatcher.Modules.AnomalyDetector.Infrastructure.SpreadSheet
{
    public class SpreadSheet
    {
        private readonly SheetsService _service;

        public SpreadSheet(Authentication authentication)
        {
            // Create Google Sheets API service.
            _service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = authentication.Authenticate(),
                ApplicationName = "SpreadSheet",
            });
        }

        /// <summary>
        /// Will create completely new Spreadsheet (new file) with given name.
        /// </summary>
        /// <param name="spreadSheetName"></param>
        /// <returns></returns>
        public Spreadsheet CreateNewSpreadSheet(string spreadSheetName)
        {
            var requestBody = new Spreadsheet
            {
                Properties = new SpreadsheetProperties
                {
                    Title = spreadSheetName
                }
            };

            var request = _service.Spreadsheets.Create(requestBody);
            return request.Execute();
        }

        // add sheet to spreadsheet
        public BatchUpdateSpreadsheetResponse AddSheet(Spreadsheet spreadsheet, string sheetName)
        {
            var requestBody = new AddSheetRequest
            {
                Properties = new SheetProperties
                {
                    Title = sheetName
                }
            };
            var batchUpdateSpreadsheetRequest = new BatchUpdateSpreadsheetRequest();
            batchUpdateSpreadsheetRequest.Requests = new List<Request>();
            batchUpdateSpreadsheetRequest.Requests.Add(
                new Request
                {
                    AddSheet = requestBody
                });
            var request = _service.Spreadsheets.BatchUpdate(batchUpdateSpreadsheetRequest, spreadsheet.SpreadsheetId);
            return request.Execute();
        }

        // check if sheet exists
        public bool SheetExists(Spreadsheet spreadsheet, string sheetName)
        {
            var request = _service.Spreadsheets.Get(spreadsheet.SpreadsheetId);
            var response = request.Execute();
            var sheets = response.Sheets;
            foreach (var sheet in sheets)
            {
                if (sheet.Properties.Title == sheetName)
                {
                    return true;
                }
            }

            return false;
        }

        public Spreadsheet GetSpreadSheet(string spreadSheetId)
        {
            var request = _service.Spreadsheets.Get(spreadSheetId);
            return request.Execute();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="spreadsheet">
        /// Spreadsheet object
        /// </param>
        /// <param name="range">
        /// Range of cells to update
        /// example: SignPost!F5 or SheetName!F5:F7
        /// </param>
        /// <param name="rangeBody">
        /// Body of cells to update
        /// </param>
        /// <returns></returns>
        public UpdateValuesResponse UpdateSpreadSheet(Spreadsheet spreadsheet, ValueRangeDto valueRangeDto, string sheetname)
        {
            // String spreadsheetId2 = "1jAXNR82a8SyPw2IUkDBQzWfaS3VzrpIVQJeWcESimf4";
            // String range2 = "SignPost!F5";  // update cell F5
            // var rangeBody = new ValueRange
            // {
            //     MajorDimension = "COLUMNS" //"ROWS";//COLUMNS
            // };
            //
            // var oblist = new List<object>() { "My Cell Text" };
            // rangeBody.Values = new List<IList<object>> { oblist };

            // check if rangeBody.Range is not empty

            var range = GetRange(valueRangeDto, sheetname);

            SpreadsheetsResource.ValuesResource.UpdateRequest update =
                _service.Spreadsheets.Values.Update(valueRangeDto.ValueRange, spreadsheet.SpreadsheetId, range);
            update.ValueInputOption = SpreadsheetsResource.ValuesResource.UpdateRequest.ValueInputOptionEnum.RAW;
            return update.Execute();
        }

        // create ValueRange object from IEnumerable<T>
        public ValueRangeDto CreateValueRange<T>(IEnumerable<T> values, bool addColumnNames = true)
        {
            var valueRange = new ValueRange();
            var maxColumnCount = 0;
            var rowCount = 0;
            valueRange.MajorDimension = "ROWS"; // ROWS is default
            // add values as rows
            var rows = new List<IList<object>>();
            if (addColumnNames)
            {
                var headerRow = new List<object>();
                foreach (var prop in typeof(T).GetProperties())
                {
                    headerRow.Add(prop.Name);
                }

                rows.Add(headerRow);
                rowCount++;
            }

            foreach (var value in values)
            {
                var cellsInRow = new List<object>();
                var columnCount = 0;
                foreach (var prop in value.GetType().GetProperties())
                {
                    cellsInRow.Add(prop.GetValue(value) ?? throw new ValueRangeNullException());
                    columnCount++;
                }

                rows.Add(cellsInRow);
                maxColumnCount = Math.Max(maxColumnCount, columnCount);
                rowCount++;
            }

            valueRange.Values = rows;
            return new ValueRangeDto(valueRange, rowCount, maxColumnCount);
        }

        public string GetRange(ValueRangeDto valueRangeDto, string sheetName)
        {
            if (!valueRangeDto.IsValid()) {
                return  GetRange(valueRangeDto.ValueRange.Values, sheetName);
            }
            var range = sheetName + "!A1:" + GetColumnName(valueRangeDto.ColumnCount) + valueRangeDto.RowCount;
            return range;
        }

        //compute the range of cells to update from IEnumerable<T>
        private string GetRange<T>(IEnumerable<T> data, string sheetName)
        {
            var maxRow = data.Count();
            // for every property in T, get the columns count where the count is highest;
            var maxColumn = data.Select(x => x.GetType().GetProperties().Count()).Max();
            var range = sheetName + "!A1:" + GetColumnName(maxColumn) + maxRow;
            return range;
        }

        // get column name from number
        private string GetColumnName(int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int) ((dividend - modulo) / 26);
            }

            return columnName;
        }


        public ValueRange? GetValuesInRange(Spreadsheet spreadsheet, string range)
        {
            SpreadsheetsResource.ValuesResource.GetRequest request =
                _service.Spreadsheets.Values.Get(spreadsheet.SpreadsheetId, range);
            return request.Execute();
        }

        public BatchUpdateSpreadsheetResponse DeleteSheet(Spreadsheet spreadsheet, Sheet sheet)
        {
            var batchUpdateSpreadsheetRequest = new BatchUpdateSpreadsheetRequest();
            batchUpdateSpreadsheetRequest.Requests = new List<Request>();
            batchUpdateSpreadsheetRequest.Requests.Add(
                new Request
                {
                    DeleteSheet = new DeleteSheetRequest
                    {
                        SheetId = sheet.Properties.SheetId,
                        ETag = sheet.ETag
                    }
                });
            var request = _service.Spreadsheets.BatchUpdate(batchUpdateSpreadsheetRequest, spreadsheet.SpreadsheetId);
            return request.Execute();
        }
    }
}