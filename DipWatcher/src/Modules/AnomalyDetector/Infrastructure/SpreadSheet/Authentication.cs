using System;
using System.IO;
using System.Threading;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Sheets.v4;
using Google.Apis.Util.Store;

namespace DipWatcher.Modules.AnomalyDetector.Infrastructure.SpreadSheet
{
    public class Authentication
    {
        //authentication to google sheets
        // apikey: AIzaSyDI-n2XNNcvjPX9DyH8hJ_5KlcStM3kfPA
        public UserCredential Authenticate()
        {
            using (var stream = new FileStream("client_secret.json", FileMode.Open, FileAccess.Read))
            {
                // credPath =  /home/david/
                var credPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                //  /home/david/.credentials/sheets.googleapis.com-dotnet-quickstart.json
                credPath = Path.Combine(credPath, ".credentials/sheets.googleapis.com-dotnet-quickstart.json");

                var credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    new[] { SheetsService.Scope.Spreadsheets },
                    "dipWatcher",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                Console.WriteLine("Credential file saved to: " + credPath);
                return credential;
            }
        }
    }
}