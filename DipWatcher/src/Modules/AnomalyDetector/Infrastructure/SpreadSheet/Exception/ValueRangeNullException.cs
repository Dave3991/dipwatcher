namespace DipWatcher.Modules.AnomalyDetector.Infrastructure.SpreadSheet.Exception
{
    public class ValueRangeNullException : System.InvalidOperationException
    {
        public ValueRangeNullException()
        {
        }

        public ValueRangeNullException(string message) : base(message)
        {
        }

        public ValueRangeNullException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}