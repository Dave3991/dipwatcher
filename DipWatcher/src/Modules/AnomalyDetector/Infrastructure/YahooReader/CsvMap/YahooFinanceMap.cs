using CsvHelper.Configuration;
using DipWatcher.Modules.AnomalyDetector.Infrastructure.YahooReader.Dto;

namespace DipWatcher.Modules.AnomalyDetector.Infrastructure.YahooReader.CsvMap
{
    public sealed class YahooFinanceMap : ClassMap<StockDatetimePriceDto>
    {
        public YahooFinanceMap()
        {
            Map(m => m.Date).Name("Date");
            Map(m => m.Open).Name("Open");
            Map(m => m.High).Name("High");
            Map(m => m.Low).Name("Low");
            Map(m => m.Close).Name("Close");
            Map(m => m.AdjClose).Name("Adj Close");
            Map(m => m.Volume).Name("Volume");
        }
    }
}