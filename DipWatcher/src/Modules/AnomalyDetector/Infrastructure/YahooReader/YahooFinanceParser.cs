using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.Configuration;
using DipWatcher.Modules.AnomalyDetector.Infrastructure.YahooReader.CsvMap;
using DipWatcher.Modules.AnomalyDetector.Infrastructure.YahooReader.Dto;
using Microsoft.AspNetCore.WebUtilities;

namespace DipWatcher.Modules.AnomalyDetector.Infrastructure.YahooReader
{
    public class YahooFinanceParser
    {
        public IEnumerable<StockDatetimePriceDto> LoadJson(string stockSymbol)
        {
            // get timestamp before 3 months
            var dateTime = DateTime.Now.AddMonths(-3);
            //datetime minus 3 months to unix timestamp
            var timestampFrom = (long)(dateTime.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            var timeStampNow = (long)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            using var httpClient = new HttpClient();
            using var request = new HttpRequestMessage(
                new HttpMethod("GET"),
                new Uri($"https://query1.finance.yahoo.com/v7/finance/download/{stockSymbol}?period1={timestampFrom}&period2={timeStampNow}&interval=1d&events=history&includeAdjustedClose=true")
                );
            request.Headers.TryAddWithoutValidation("Upgrade-Insecure-Requests", "1");

            var response = httpClient.Send(request);
            if (response.Content is null)
            {
                throw new Exception("Content is null");
            }

            var stream = response.Content.ReadAsStream();
            if (stream == null)
            {
                throw new Exception("stream is null");
            }
            TextReader reader = new HttpRequestStreamReader(stream, Encoding.UTF8);
            // reader.ReadLine();
            var csvConfiguration = new CsvConfiguration(CultureInfo.CurrentCulture);
            var csvParser = new CsvParser(reader, csvConfiguration);
            var csvReader = new CsvReader(csvParser);
            csvReader.Context.RegisterClassMap<YahooFinanceMap>();
            var records = csvReader.GetRecords<StockDatetimePriceDto>();
            return records;

        }
    }
}