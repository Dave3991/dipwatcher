using System.Collections.Generic;
using DipWatcher.Modules.AnomalyDetector.Domain.ValueObject;
using MediatR;

namespace DipWatcher.Modules.AnomalyDetector.Application.Operations
{
    public class ChangePointDetectExternalDataCommand : IRequest<IEnumerable<TimeSeriesDataPrediction>?>
    {
        public IEnumerable<TimeSeriesData> TimeSeriesDatas { get; }

        public ChangePointDetectExternalDataCommand(IEnumerable<TimeSeriesData> timeSeriesDatas)
        {
            TimeSeriesDatas = timeSeriesDatas;
        }
    }
}
