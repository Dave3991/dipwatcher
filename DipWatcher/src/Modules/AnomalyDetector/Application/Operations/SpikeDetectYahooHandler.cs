using System;
using System.Collections.Generic;
using System.Linq;
using DipWatcher.Modules.AnomalyDetector.Application.Service;
using DipWatcher.Modules.AnomalyDetector.Domain;
using DipWatcher.Modules.AnomalyDetector.Domain.Adapter.DTO;
using DipWatcher.Modules.AnomalyDetector.Domain.Case;
using DipWatcher.Modules.AnomalyDetector.Domain.Entity;
using DipWatcher.Modules.AnomalyDetector.Domain.ValueObject;
using DipWatcher.Modules.AnomalyDetector.Infrastructure.Database;
using DipWatcher.Modules.AnomalyDetector.Infrastructure.SpreadSheet;
using DipWatcher.Modules.AnomalyDetector.Infrastructure.YahooReader;
using DipWatcher.Modules.AnomalyDetector.Infrastructure.YahooReader.Dto;
using Google.Apis.Sheets.v4.Data;
using MediatR;
using Microsoft.ML.Transforms.TimeSeries;
using Serilog;

namespace DipWatcher.Modules.AnomalyDetector.Application.Operations
{
    public class SpikeDetectYahooHandler : RequestHandler<SpikeDetectYahooCommand, IEnumerable<PredictionResultEntity>?>
    {
        private readonly SpikeDetectionCase _spikeDetection;

        private readonly SpikeDetectionSsaCase _spikeDetectionSsaCase;

        private readonly YahooFinanceParser _yahooFinanceParser;

        private readonly ConvertInfrastructureData _convertInfrastructure;

        private readonly IPredictionResultRepository _predictionResultRepository;
        private readonly SpreadSheet _spreadSheet;

        private readonly Spreadsheet _stockSpreadSheet;
        private ILogger _logger;

        public SpikeDetectYahooHandler(
            YahooFinanceParser yahooFinanceParser,
            SpikeDetectionCase spikeDetection,
            SpikeDetectionSsaCase spikeDetectionSsaCase,
            TweetSender.Case.TweetSenderCase tweetSenderCase,
            ConvertInfrastructureData convertInfrastructure,
            IPredictionResultRepository predictionResultRepository,
            SpreadSheet spreadSheet, ILogger logger)
        {
            _yahooFinanceParser = yahooFinanceParser;
            _spikeDetection = spikeDetection;
            _spikeDetectionSsaCase = spikeDetectionSsaCase;
            _convertInfrastructure = convertInfrastructure;
            _predictionResultRepository = predictionResultRepository;
            _spreadSheet = spreadSheet;
            _logger = logger;
            _stockSpreadSheet = _spreadSheet.GetSpreadSheet("1jAXNR82a8SyPw2IUkDBQzWfaS3VzrpIVQJeWcESimf4");
        }

        [Obsolete]
        protected override IEnumerable<PredictionResultEntity> Handle(SpikeDetectYahooCommand request)
        {
            var config = new SpikeDetectionCaseConfig(
                request.DetectRequest.StockSymbol,
                request.DetectRequest.AnomalySide,
                request.DetectRequest.Confidence
            );
            var data = _yahooFinanceParser.LoadJson(request.DetectRequest.StockSymbol);
            var timeSeriesDatas = ConvertToTimeSeriesData(data, request.DetectRequest.AnomalySide);

            if (request.DetectRequest.UseSsaMethod)
            {
                //return _spikeDetectionSsaCase.DetectSpike(timeSeriesDatas, config);
            }

            var predictionResults = _spikeDetection.DetectSpike(timeSeriesDatas, config);
            var predictionResultEntities = predictionResults.ToList();
            UpdateSpreadSheetStockValues(predictionResultEntities);
            _predictionResultRepository.AddOrUpdate(predictionResultEntities);
            return predictionResultEntities;
        }

        private void UpdateSpreadSheetStockValues(IEnumerable<PredictionResultEntity> predictionResultEntities)
        {
            // get first entity in predictionResultEntities
            var sheetName = predictionResultEntities.First().ResultId;
            if (!_spreadSheet.SheetExists(_stockSpreadSheet, sheetName))
            {
                _spreadSheet.AddSheet(_stockSpreadSheet, sheetName);
            }

            var rangeDto = _spreadSheet.CreateValueRange(predictionResultEntities);
            var signPostTickerRow = new SignPostTickerRow(
                predictionResultEntities.First().ResultId,
                DateTime.Now,
                predictionResultEntities.Last().IsAnomaly,
                predictionResultEntities.Last().Value);
           var signPostValueRange = UpdateSpreadSheetSignPost(signPostTickerRow);
           _spreadSheet.UpdateSpreadSheet(_stockSpreadSheet, signPostValueRange, "SignPost");
           _spreadSheet.UpdateSpreadSheet(_stockSpreadSheet, rangeDto, sheetName);
        }

        /// <summary>
        /// This method finds stock tiker or creates new row with ticker and updates row with values
        /// </summary>
        /// <param name="signPostTickerRow"></param>
        /// <returns></returns>
        private ValueRangeDto UpdateSpreadSheetSignPost(SignPostTickerRow signPostTickerRow)
        {
            var sheetValueRange = _spreadSheet.GetValuesInRange(_stockSpreadSheet, "SignPost!A1:Z500");
            var rowCount = 0;
            var maxColumnCount = 0;
            _logger.Information("Major dimension is: " + sheetValueRange.MajorDimension);
            var sheetData = sheetValueRange.Values;
            // get index where is given string in first column
            // TODO fix - if ticker is avaiable
            var isTickerAvaiable = sheetData.Select((x, i) => sheetData[i].Contains(signPostTickerRow.Ticker)).Any(x => (x ? false : true));
            // if ticker not found, add row with ticker
            if (false)
            {
                var columnTicker = new List<object>{signPostTickerRow.Ticker};
                var columnUpdateTime = new List<object>{signPostTickerRow.UpdateTime};
                var columnIsDipDetected= new List<object>{signPostTickerRow.IsDipDetected};
                var columnPrice = new List<object>{signPostTickerRow.Price};
                sheetData.Add(columnTicker);
                sheetData.Add(columnUpdateTime);
                sheetData.Add(columnIsDipDetected);
                sheetData.Add(columnPrice);
            }
            // update row
            else
            {
                var rowIndex = 0;
                foreach(var row in sheetData)
                {
                    var columnCount = 0;
                    foreach (var column in row)
                    {
                        if (column.ToString() == signPostTickerRow.Ticker)
                        {
                            //clear row, so we can update it
                            sheetData[rowIndex].Clear();
                            sheetData[rowIndex].Insert(0, signPostTickerRow.Ticker);
                            sheetData[rowIndex].Insert(1, signPostTickerRow.UpdateTime.ToString());
                            sheetData[rowIndex].Insert(2, signPostTickerRow.IsDipDetected);
                            sheetData[rowIndex].Insert(3, signPostTickerRow.Price);
                            break;
                        }

                        columnCount++;
                    }
                    maxColumnCount = columnCount > maxColumnCount ? columnCount : maxColumnCount;
                    rowIndex++;
                    rowCount++;
                }
            }

            var valueRange = new ValueRange();
            valueRange.Values = sheetData.ToList();
            // columnCount + 3 becasue we add 3 columns
            return new ValueRangeDto(valueRange, rowCount, maxColumnCount + 3);
        }


        private IEnumerable<TimeSeriesData> ConvertToTimeSeriesData(IEnumerable<StockDatetimePriceDto> stockDatetimePriceDtos, AnomalySide anomalySide)
        {
            switch (anomalySide)
            {
                case AnomalySide.Negative:
                    {
                        return _convertInfrastructure.ConvertToTimeSeriesDataLowPrice(stockDatetimePriceDtos);
                    }

                case AnomalySide.Positive:
                    {
                        return _convertInfrastructure.ConvertToTimeSeriesDataHighPrice(stockDatetimePriceDtos);
                    }
            }

            throw new InvalidOperationException();
        }
    }


}