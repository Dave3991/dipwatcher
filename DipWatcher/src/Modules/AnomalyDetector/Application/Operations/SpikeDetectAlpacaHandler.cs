using System;
using System.Collections.Generic;
using System.Linq;
using Alpaca.Markets;
using DipWatcher.Modules.AnomalyDetector.Application.Service;
using DipWatcher.Modules.AnomalyDetector.Domain.Case;
using DipWatcher.Modules.AnomalyDetector.Domain.Entity;
using DipWatcher.Modules.AnomalyDetector.Domain.ValueObject;
using DipWatcher.Modules.AnomalyDetector.Infrastructure.AlpacaReader;
using MediatR;
using Microsoft.ML.Transforms.TimeSeries;

namespace DipWatcher.Modules.AnomalyDetector.Application.Operations
{
    public class SpikeDetectAlpacaHandler : RequestHandler<SpikeDetectAlpacaCommand, IEnumerable<PredictionResultEntity>?>
    {
        private readonly SpikeDetectionCase _spikeDetection;

        private readonly SpikeDetectionSsaCase _spikeDetectionSsaCase;

        private readonly AlpacaParser _alpacaParser;

        private readonly ConvertInfrastructureData _convertInfrastructure;


        public SpikeDetectAlpacaHandler(
            SpikeDetectionCase spikeDetection,
            SpikeDetectionSsaCase spikeDetectionSsaCase,
            AlpacaParser alpacaParser,
            ConvertInfrastructureData convertInfrastructure)
        {
            _spikeDetection = spikeDetection;
            _spikeDetectionSsaCase = spikeDetectionSsaCase;
            _alpacaParser = alpacaParser;
            _convertInfrastructure = convertInfrastructure;
        }

        [Obsolete]
        protected override IEnumerable<PredictionResultEntity> Handle(SpikeDetectAlpacaCommand request)
        {
            var config = new SpikeDetectionCaseConfig(
                request.DetectRequest.StockSymbol,
                request.DetectRequest.AnomalySide,
                request.DetectRequest.Confidence
                );
            var timeSeriesDatas =
                ConvertToTimeSeriesData(_alpacaParser.LoadJson(request.DetectRequest.StockSymbol), request.DetectRequest.AnomalySide);

            if (request.DetectRequest.UseSsaMethod)
            {
                return _spikeDetectionSsaCase.DetectSpike(timeSeriesDatas, config);
            }

            return _spikeDetection.DetectSpike(timeSeriesDatas, config);
        }

        private IEnumerable<TimeSeriesData> ConvertToTimeSeriesData(IReadOnlyList<IBar> timeSeriesDatas, AnomalySide anomalySide)
        {
            switch (anomalySide)
            {
                case AnomalySide.Negative:
                    {
                        return _convertInfrastructure.ConvertToTimeSeriesDataLowPrice(timeSeriesDatas);
                    }

                case AnomalySide.Positive:
                    {
                        return _convertInfrastructure.ConvertToTimeSeriesDataHighPrice(timeSeriesDatas);
                    }
            }

            throw new InvalidOperationException();
        }

    }
}