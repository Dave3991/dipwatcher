using System.Collections.Generic;
using DipWatcher.Modules.AnomalyDetector.Domain.Entity;
using DipWatcher.Modules.AnomalyDetector.Domain.ValueObject;
using MediatR;

namespace DipWatcher.Modules.AnomalyDetector.Application.Operations
{
    public class SpikeDetectExternalDataCommand : IRequest<IEnumerable<PredictionResultEntity>?>
    {
        public string ResultId { get; }
        public IEnumerable<TimeSeriesData> TimeSeriesDatas { get; }
        public SpikeDetectExternalDataCommand(string resultId, IEnumerable<TimeSeriesData> timeSeriesDatas)
        {
            ResultId = resultId;
            TimeSeriesDatas = timeSeriesDatas;
        }
    }
}
