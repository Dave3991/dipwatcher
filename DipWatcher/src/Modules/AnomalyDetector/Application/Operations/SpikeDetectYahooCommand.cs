using System.Collections.Generic;
using DipWatcher.Modules.AnomalyDetector.Domain.Entity;
using DipWatcher.Modules.AnomalyDetector.Domain.ValueObject;
using DipWatcher.Modules.AnomalyDetector.UI.Dto;
using MediatR;

namespace DipWatcher.Modules.AnomalyDetector.Application.Operations
{
    public class SpikeDetectYahooCommand : IRequest<IEnumerable<PredictionResultEntity>?>
    {
        public ChangePointDetectRequest DetectRequest { get; }

        public SpikeDetectYahooCommand(ChangePointDetectRequest detectRequest)
        {
            DetectRequest = detectRequest;
        }


    }
}