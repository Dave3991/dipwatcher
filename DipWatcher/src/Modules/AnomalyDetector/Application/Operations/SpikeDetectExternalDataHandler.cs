using System.Collections.Generic;
using DipWatcher.Modules.AnomalyDetector.Domain.Case;
using DipWatcher.Modules.AnomalyDetector.Domain.Entity;
using DipWatcher.Modules.AnomalyDetector.Domain.ValueObject;
using MediatR;
using Microsoft.ML.Transforms.TimeSeries;

namespace DipWatcher.Modules.AnomalyDetector.Application.Operations
{
    public class SpikeDetectExternalDataHandler : RequestHandler<SpikeDetectExternalDataCommand, IEnumerable<PredictionResultEntity>?>
    {
        private readonly SpikeDetectionCase _spikeDetectionCase;

        public SpikeDetectExternalDataHandler(SpikeDetectionCase spikeDetectionCase)
        {
            _spikeDetectionCase = spikeDetectionCase;
        }

        protected override IEnumerable<PredictionResultEntity> Handle(SpikeDetectExternalDataCommand request)
        {
            var config = new SpikeDetectionCaseConfig(request.ResultId, AnomalySide.TwoSided, 95.0);
            return _spikeDetectionCase.DetectSpike(request.TimeSeriesDatas, config);
        }
    }
}
