using System.Collections.Generic;
using DipWatcher.Modules.AnomalyDetector.Domain.Case;
using DipWatcher.Modules.AnomalyDetector.Domain.ValueObject;
using MediatR;

namespace DipWatcher.Modules.AnomalyDetector.Application.Operations
{
    public class ChangePointDetecExternalDatatHandler : RequestHandler<ChangePointDetectExternalDataCommand, IEnumerable<TimeSeriesDataPrediction>?>
    {
        private readonly ChangePointDetectionCase _changePointDetectionCase;
        public ChangePointDetecExternalDatatHandler(ChangePointDetectionCase changePointDetectionCase)
        {
            _changePointDetectionCase = changePointDetectionCase;
        }

        protected override IEnumerable<TimeSeriesDataPrediction>? Handle(ChangePointDetectExternalDataCommand request)
        {
            return _changePointDetectionCase.DetectChangePoint(request.TimeSeriesDatas);
        }
    }
}
