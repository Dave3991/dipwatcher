using System.Collections.Generic;
using DipWatcher.Modules.AnomalyDetector.Domain.Entity;
using DipWatcher.Modules.AnomalyDetector.Domain.ValueObject;
using DipWatcher.Modules.AnomalyDetector.UI.Dto;
using MediatR;
using Microsoft.ML.Transforms.TimeSeries;

namespace DipWatcher.Modules.AnomalyDetector.Application.Operations
{
    public class SpikeDetectAlpacaCommand : IRequest<IEnumerable<PredictionResultEntity>?>
    {
        public ChangePointDetectRequest DetectRequest { get; }

        public SpikeDetectAlpacaCommand(ChangePointDetectRequest detectRequest)
        {
            DetectRequest = detectRequest;
        }


    }
}