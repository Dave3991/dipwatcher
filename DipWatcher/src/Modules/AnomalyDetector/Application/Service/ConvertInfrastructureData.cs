using System.Collections.Generic;
using Alpaca.Markets;
using DipWatcher.Modules.AnomalyDetector.Domain.ValueObject;
using System.Linq;
using DipWatcher.Modules.AnomalyDetector.Infrastructure.YahooReader.Dto;

namespace DipWatcher.Modules.AnomalyDetector.Application.Service
{
    public class ConvertInfrastructureData
    {
        public IEnumerable<TimeSeriesData> ConvertToTimeSeriesDataLowPrice(IReadOnlyList<IBar> barData)
        {
            return
                from b in barData
                select new TimeSeriesData(
                    b.TimeUtc,
                    (float)b.Low
                );
        }

        public IEnumerable<TimeSeriesData> ConvertToTimeSeriesDataHighPrice(IReadOnlyList<IBar> barData)
        {
            return
                from b in barData
                select new TimeSeriesData(
                    b.TimeUtc,
                    (float)b.High
                );
        }

        public IEnumerable<TimeSeriesData> ConvertToTimeSeriesDataLowPrice(IEnumerable<StockDatetimePriceDto> stockDatetimePriceDtos)
        {
            return
                from stockDatetimePriceDto in stockDatetimePriceDtos
                select new TimeSeriesData(
                    stockDatetimePriceDto.Date,
                    (float)stockDatetimePriceDto.Low
                );
        }

        public IEnumerable<TimeSeriesData> ConvertToTimeSeriesDataHighPrice(IEnumerable<StockDatetimePriceDto> stockDatetimePriceDtos)
        {
            return
                from stockDatetimePriceDto in stockDatetimePriceDtos
                select new TimeSeriesData(
                    stockDatetimePriceDto.Date,
                    (float)stockDatetimePriceDto.High
                );
        }
    }
}