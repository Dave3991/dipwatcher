using DipWatcher.Modules.MessageProvider.Domain.Dto;
using DipWatcher.Modules.MessageProvider.Domain.Entity;
using MediatR;

namespace DipWatcher.Modules.MessageProvider.Application.Operations
{
    public class GetMessageQuery : IRequest<MessageRecord>
    {
        public TimeSeriesSpikeDto Spike { get; }

        public GetMessageQuery(TimeSeriesSpikeDto spike)
        {
            Spike = spike;
        }
    }
}