using DipWatcher.Modules.MessageProvider.Domain.Case;
using DipWatcher.Modules.MessageProvider.Domain.Entity;
using MediatR;

namespace DipWatcher.Modules.MessageProvider.Application.Operations
{
    public class GetMessageHandler : RequestHandler<GetMessageQuery, MessageRecord>
    {
        private readonly MessageCase _messageCase;

        public GetMessageHandler(MessageCase messageCase)
        {
            _messageCase = messageCase;
        }

        protected override MessageRecord Handle(GetMessageQuery request)
        {
            return _messageCase.GetMessage(request.Spike);
        }
    }
}