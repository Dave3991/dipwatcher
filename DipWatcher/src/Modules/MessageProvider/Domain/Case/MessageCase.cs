using System;
using DipWatcher.Modules.AnomalyDetector.UI.Dto;
using DipWatcher.Modules.MessageProvider.Domain.Dto;
using DipWatcher.Modules.MessageProvider.Domain.Entity;
using Microsoft.ML.Transforms.TimeSeries;
using Tweetinvi.Core.Models;

namespace DipWatcher.Modules.MessageProvider.Domain.Case
{
    public class MessageCase
    {
        public MessageRecord GetMessage(TimeSeriesSpikeDto lastSpike)
        {
            switch (lastSpike.AnomalySide)
            {
                case AnomalySide.Negative:
                    {
                        return new MessageRecord($"Dip for ${lastSpike.StockSymbol} at {Math.Round(lastSpike.Value, 2)} in {lastSpike.DateTime}");
                    }
                case AnomalySide.Positive:
                    {
                        return new MessageRecord($"Peak for ${lastSpike.StockSymbol} at {Math.Round(lastSpike.Value, 2)} in {lastSpike.DateTime}");
                    }
                default:
                    return new MessageRecord($"Unknown settings for message - stockSymbol: {lastSpike.StockSymbol}, anomalySide: {lastSpike.AnomalySide}");

            }
        }
    }
}