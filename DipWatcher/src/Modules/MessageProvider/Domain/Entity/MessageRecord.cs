namespace DipWatcher.Modules.MessageProvider.Domain.Entity
{
    public record MessageRecord
    {
        public string Message { get; }

        public MessageRecord(string message)
        {
            Message = message;
        }
    }
}