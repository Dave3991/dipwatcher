using System;
using Microsoft.ML.Transforms.TimeSeries;

namespace DipWatcher.Modules.MessageProvider.Domain.Dto
{
    public record TimeSeriesSpikeDto
    {
        public string StockSymbol { get; }
        public AnomalySide AnomalySide { get; }
        public DateTime DateTime { get; }

        public double Value { get; }

        public TimeSeriesSpikeDto(DateTime dateTime, double value, AnomalySide anomalySide, string stockSymbol)
        {
            DateTime = dateTime;
            Value = value;
            AnomalySide = anomalySide;
            StockSymbol = stockSymbol;
        }

    }
}