using DipWatcher.Modules.AnomalyDetector.Application.Service;
using DipWatcher.Modules.AnomalyDetector.Domain.Case;
using DipWatcher.Modules.AnomalyDetector.Domain.Services;
using DipWatcher.Modules.AnomalyDetector.Infrastructure.AlpacaReader;
using DipWatcher.Modules.AnomalyDetector.Infrastructure.FileWriter;
using DipWatcher.Modules.AnomalyDetector.Infrastructure.YahooReader;
using DipWatcher.Modules.MessageProvider.Domain.Case;
using Microsoft.Extensions.DependencyInjection;

namespace DipWatcher.Modules.MessageProvider.Setup
{
    public static class Services
    {
        public static void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<MessageCase>();
        }
    }
}
