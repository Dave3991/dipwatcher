using System.Threading.Tasks;
using MediatR;
using Tweetinvi.Models;

namespace DipWatcher.Modules.TweetSender.Application.Operations
{
    public class SendTweetCommand : IRequest<Task<ITweet>>
    {
        public string TweetMessage { get; }

        public SendTweetCommand(string tweetMessage)
        {
            this.TweetMessage = tweetMessage;
        }
    }
}