using System.Threading.Tasks;
using DipWatcher.Modules.TweetSender.Case;
using MediatR;
using Tweetinvi.Models;

namespace DipWatcher.Modules.TweetSender.Application.Operations
{
    public class SendTweetHandler : RequestHandler<SendTweetCommand, Task<ITweet>>
    {
        private readonly TweetSenderCase _tweetSenderCase;

        public SendTweetHandler(TweetSenderCase tweetSenderCase)
        {
            _tweetSenderCase = tweetSenderCase;
        }

        protected override Task<ITweet> Handle(SendTweetCommand request)
        {
            return _tweetSenderCase.PublishTweetMessage(request.TweetMessage);
        }
    }
}