using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Tweetinvi;
using Tweetinvi.Models;

namespace DipWatcher.Modules.TweetSender.Case
{
    public class TweetSenderCase
    {
        private static readonly string ConsumerKey = Environment.GetEnvironmentVariable("ConsumerKey");
        private static readonly string ConsumerSecret = Environment.GetEnvironmentVariable("ConsumerSecret");
        private static readonly string AccessToken = Environment.GetEnvironmentVariable("AccessToken");
        private static readonly string AccessTokenSecret = Environment.GetEnvironmentVariable("AccessTokenSecret");

        private readonly TwitterClient _twitterClient;

        private readonly ILogger<TweetSenderCase> _logger;
        public TweetSenderCase(ILogger<TweetSenderCase> logger)
        {
            _logger = logger;
            _twitterClient = CreateTwitterClient();
        }

        private TwitterClient CreateTwitterClient()
        {
            return new TwitterClient(ConsumerKey, ConsumerSecret, AccessToken, AccessTokenSecret);
        }

        public async Task<ITweet?> PublishTweetMessage(string message)
        {
            // await this.AuthenticateClient();
            try
            {

                var user = await _twitterClient.Users.GetAuthenticatedUserAsync();
                _logger.LogInformation("Logged: {@User}", user);
                // var tweet = await user.Client.Tweets.PublishTweetAsync("Hello tweetinvi world!");
                var tweet = await _twitterClient.Tweets.PublishTweetAsync(message);
                _logger.LogInformation("Tweeted: {Message}", message);
                return tweet;

            }
            catch (Exception e)
            {
                _logger.LogCritical(e.Message, e);
            }

            return null;
        }

    }
}