using System.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DipWatcher.Infrastructure.Database.Setup
{
    public static class Services
    {
        public static void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<DipWatcherDbContext>(options =>
                options.UseNpgsql(configuration.GetConnectionString("postgreConnection")));
        }
    }
}