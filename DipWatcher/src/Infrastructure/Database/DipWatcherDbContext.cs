using System.ComponentModel.DataAnnotations.Schema;
using DipWatcher.Modules.AnomalyDetector.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace DipWatcher.Infrastructure.Database
{
    public class DipWatcherDbContext: DbContext
    {
        private readonly ILoggerFactory _loggerFactory;

        public DipWatcherDbContext(DbContextOptions<DipWatcherDbContext> options, ILoggerFactory loggerFactor): base(options)
        {
            _loggerFactory = loggerFactor;
        }

        public DbSet<StockTimeSeriesEntity> stock_time_series { get; init; }

        public DbSet<PredictionResultEntity> PredictionResult { get; init; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.TrackAll);
            optionsBuilder.EnableDetailedErrors(true);
            optionsBuilder.EnableSensitiveDataLogging(true);
            optionsBuilder.UseLoggerFactory(_loggerFactory);
            // optionsBuilder.LogTo(Console.WriteLine);
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<StockTimeSeriesEntity>()
                .HasKey(x => new {x.StockSymbol, x.DateTime});

            modelBuilder.Entity<PredictionResultEntity>()
                .HasKey(x => new {ResultId = x.ResultId, x.DateTime});
            base.OnModelCreating(modelBuilder);
        }
        
        
        
    }
}