SHELL := /bin/bash
MIN_MAKE_VERSION := 3.82

.DEFAULT_GOAL:=help

##@ Development
.PHONY:
up: ## start the application containers - docker-compose.debug.yml
	docker-compose -f docker-compose.yml up dipwatcher
up-args: ## Start the application containers (use "args=" to supply custom arguments) 
	docker-compose -f docker-compose.yml up $(args)
down: ## Stop and remove the application containers

##@ Build
build: ## docker build
	docker build --tag="registry.gitlab.com/dave3991/dipwatcher:production" --no-cache --file ./Dockerfile .

push: ## docker push to my gitlab registry
	docker push registry.gitlab.com/dave3991/dipwatcher:production

##@ Lint
lint: lint-yaml lint-dockerfile lint-dotnet ## Run all linters

lint-dockerfile: ## dockerfile lint
	docker run --rm -i hadolint/hadolint:2.8.0 < Dockerfile

lint-yaml: ## yaml lint
	yamllint .

lint-dotnet: ## dotnet lint
	dotnet format --severity=info --verbosity=d --verify-no-changes

##@ Test
test: ## Run all tests
	dotnet test

##@ Migrations
migrate-init: ## init migrations
	@dotnet ef migrations add InitialCreate --project ./DipWatcher

migrate-up: ## apply migrations
	@dotnet ef database update --project ./DipWatcher

migrate-down: ## revert migrations
	@dotnet ef database update --project ./DipWatcher --target 0

migrate: ## run migrations TODO: test it
	dotnet ef migrations add $(migration) -c DipWatcherContext -o Data/Migrations
	dotnet ef database update -c DipWatcherContext

##@ Fix
fix-dotnet-lint: ## fix lint errors
	dotnet format --severity=info --verbosity=d

##@ Help
.PHONY: help
help: ## Show all available commands (you are looking at it)
	@awk 'BEGIN {FS = ":.*##"; printf "Usage: make \033[36m<target>\033[0m\n"} /^[a-zA-Z0-9_-]+:.*?##/ { printf "  \033[36m%-25s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)
